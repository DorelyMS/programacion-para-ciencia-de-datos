;;; publish.el --- Publish reveal.js presentation from Org file
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2020 Adolfo De Unánue
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; License: GPL-3.0-or-later

;;; Commentary:

;; Usa este archivo desde el directorio padre con el siguiente comando:
;; emacs --batch --load elisp/publish.el




(setq org-latex-compiler "xelatex"
      org-latex-pdf-process '("latexmk -g -%latex -shell-escape -outdir=%o %f")
      org-latex-packages-alist '(("" "fontspec" nil ("lualatex" "xelatex"))
                                 ("" "natbib"))
      org-export-allow-bind-keywords t)

(package-initialize)


(require 'org)
(require 'ox-extra)
(ox-extras-activate '(ignore-headlines))
(require 'ox-bibtex)
(require 'ox-reveal)

(setq-default org-confirm-babel-evaluate nil
              org-confirm-elisp-link-function nil
              org-confirm-shell-link-function nil)

(with-eval-after-load "ox-latex"
  (add-to-list 'org-latex-classes
               '("koma-article" "\\documentclass{scrartcl}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))


(defconst nanounanue-reveal-dir "/root/reveal.js")

(defun nanounanue/publish-optional-projects ()
  "Genera una lista de proyectos opcionales para `org-publish-project-alist'.
En particular \"index.org\" que se publica con `org-html-publish-to-html'
El archivo \"index.css\"  y los directorios \"audio\", \"figures\",
\"quizzes\" se publican con  `org-publish-attachment'."
  (let (result)
    (when (file-exists-p "index.org")
      (push (list "index"
                  :base-directory "."
                  :include '("index.org")
                  :exclude ".*"
                  :publishing-function '(org-html-publish-to-html)
                  :publishing-directory "./public")
            result))
    (when (file-exists-p "index.css")
      (push (list "index-css"
                  :base-directory "."
                  :include '("index.css")
                  :exclude ".*"
                  :publishing-function '(org-publish-attachment)
                  :publishing-directory "./public")
            result))
    (when (file-accessible-directory-p "audio")
      (push (list "audio"
                  :base-directory "audio"
                  :base-extension (regexp-opt '("ogg" "mp3"))
                  :publishing-directory "./public/audio"
                  :publishing-function 'org-publish-attachment)
            result))
    (when (file-accessible-directory-p "lectures/images")
      (push (list "images"
                  :base-directory "lectures/images"
                  :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
                  :publishing-directory "./public/lectures/images"
                  :publishing-function 'org-publish-attachment
                  :recursive t)
            result))
    (when (file-accessible-directory-p "quizzes")
      (push (list "quizzes"
                  :base-directory "quizzes"
                  :base-extension (regexp-opt '("js"))
                  :publishing-directory "./public/quizzes"
                  :publishing-function 'org-publish-attachment)
            result))
    result))

(defun nanounanue/publish-all (&optional project-alist)
  (interactive)
  (let ((org-confirm-babel-evaluate nil)
        (org-publish-project-alist
         (append
          (list
           (list "org-presentations"
                 :base-directory "slides"
                 :base-extension "org"
                 :with-broken-links t
                 :makeindex nil
                 :exclude "index\\|backmatter\\|config\\|course-list\\|license-template\\|imprint\\|privacy\\|README\\|CONTRIBUTING\\|CHANGELOG\\|title-slide\\|bibliography\\|pcd\\|pcd-2020"
                 :publishing-function '(org-reveal-publish-to-reveal)
                 :publishing-directory "./public/slides")
           (list "org-handouts"
                 :base-directory "lectures"
                 :base-extension "org"
                 :with-broken-links t
                 :makeindex nil
                 :exclude "index\\|backmatter\\|config\\|course-list\\|license-template\\|imprint\\|privacy\\|README\\|CONTRIBUTING\\|CHANGELOG\\|title-slide\\|bibliography\\|pcd\\|pcd-2020"
                 :publishing-function '(org-html-publish-to-html)
                 :publishing-directory "./public/lectures")
           (list "sample-quizzes"
                 :base-directory (expand-file-name "examples/quizzes" nanounanue-reveal-dir)
                 :base-extension (regexp-opt '("js"))
                 :publishing-directory "./public/quizzes"
                 :publishing-function 'org-publish-attachment)
           (list "reveal-theme"
                 :base-directory (expand-file-name "css" nanounanue-reveal-dir)
                 :base-extension 'any
                 :publishing-directory "./public/reveal.js/css/theme"
                 :publishing-function 'org-publish-attachment)
           (list "reveal-static"
                 :base-directory (expand-file-name nanounanue-reveal-dir)
                 :exclude "\\.git"
                 :base-extension 'any
                 :publishing-directory "./public/reveal.js"
                 :publishing-function 'org-publish-attachment
                 :recursive t))
                                        ;(oer-reveal-publish-plugin-projects)
          (nanounanue/publish-optional-projects)
          org-publish-project-alist
          project-alist)))
    (org-publish-all)))



;; Publish Org files.
(nanounanue/publish-all
 (list
  (list "texts"
        :base-directory "texts"
        :base-extension "org"
        :publishing-function '(org-html-publish-to-html) ;;  org-latex-publish-to-pdf
        :publishing-directory "./public/texts")
  (list "figures"
        :base-directory "assets/figuras"
        :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
        :publishing-function 'org-publish-attachment
        :recursive t
        :publishing-directory "./public/assets/figuras")
  (list "theme"
        :base-directory "css/theme"
        :base-extension (regexp-opt '("css"))
        :publishing-function 'org-publish-attachment
        :publishing-directory "./public/reveal.js/css/theme")
  (list "css"
        :base-directory "css"
        :base-extension (regexp-opt '("css"))
        :publishing-function 'org-publish-attachment
        :publishing-directory "./public/css")))

;;; publish.el ends here
