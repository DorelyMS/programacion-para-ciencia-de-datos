#+TITLE: Línea de comandos
#+DATE: Agosto 11 de 2020

#+LANGUAGE: es

#+PROPERTY: header-args :eval never-export
#+STARTUP: latexpreview
#+STARTUP: overview
#+STARTUP: hideblocks

#+EXCLUDE_TAGS: slidesonly teachersonly noexport homework appendix

#+OPTIONS: TeX:t LaTeX:t
#+OPTIONS: d:(not "TEACHERONLY" "ZOOM" "LOGBOOK")

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="/programacion-para-ciencia-de-datos/css/gtd.css" />
#+OPTIONS: email:nil num:nil author:nil tags:nil timestamp:nil toc:nil todo:t ^:nil html-scripts:nil html-style:nil


#+KEYWORDS:
#+DESCRIPTION:


* /Bash programming/                                                    :cli:

La mayor parte del tiempo usaremos el =shell=, para hacer pequeños
/scripts/, pero existen ocasiones en las cuales es necesario tratar al
=shell= como un lenguaje de programación[fn:13]

** Estructuras de datos

Las variables son declaradas

#+begin_src shell
nombre="Adolfo"
#+end_src

Nota que no hay espacios alrededor del signo de igual. El valor de la
variable se obtiene con el signo de dólares

#+begin_src shell
echo $nombre
#+end_src


Es posible definir variables que no sean escalares, llamadas arreglos (=arrays=).

#+begin_src shell
array=(abc 123 def "programming for data science")
#+end_src

Para acceder a elementos en el arreglo usa la posición e.g. =3=:

#+begin_src shell
echo ${array[3]}
#+end_src

Tambien es posible usar /globs/

#+begin_src shell
echo ${array[*]}
#+end_src

Para conocer el número de elementos del arreglo

#+begin_src shell
echo ${#array}
#+end_src

#+begin_warning
Los arreglos es un punto donde los diferentes /shells/ ejecutan
diferente.
=bash= tiene índices basados en =0=, =zsh= en =1=.
#+end_warning

** Bucles de ejecución, (/Loops/)

Los =for-loops= en =bash= tienen una estructura muy similar a los de
=python=:

#+begin_src shell
for var in iterable; do
instrucción
instrucción
...
done
#+end_src

Donde =iterable= puede construirse con /globs/

#+begin_src shell
for i in *; do
  echo $i;
done
#+end_src

#+REVEAL: split
listas,

#+begin_src shell
for i in hola adios mundo cruel; do
  echo $i;
done
#+end_src

arreglos,

#+begin_src shell
for i in $array; do
  echo $i;
done
#+end_src

#+REVEAL: split

E inclusive el horrible formato de =C=:

#+begin_src shell
for (( i = 0; i < 10; i++ )) do
    echo $i;
done
#+end_src

#+begin_info
También puedes escribirlo en una sola línea:  =for i in {a..z}; do echo $i; done=
#+end_info

** /Tricks/                                                   :handbookonly:


También es posible hacer lo siguiente:

#+begin_src shell
for i in {a..z}; do
  echo $i;
done
#+end_src


En el ejemplo anterior utilizamos /brace expansion/:

#+begin_src shell
echo {0..9}
echo {0..10..2}
echo data.{txt,csv,json,parquet}
#+end_src

** Condicionales                                              :handbookonly:

Existen dos operadores para hacer pruebas en bash: =[= y =[[=.

De preferencia usa =[[=

#+begin_src shell
[[ 1 = 1 ]]
echo $?    # Imprime el resultado del comando previo (true -> 0)
#+end_src


Existen algunos operadores para hacer comparaciones los más comunes
son =-a, -d, -z= (unarios) y =-lt, -gt, -eq, -neq= (binarios).

Teniendo los /tests/ es posible crear estructuras =if-then-else=

#+begin_src shell
if TEST-COMMANDS; then CONSEQUENT-COMMANDS; fi
#+end_src


** Funciones

Las funciones son símbolo de buena programación, ya que encapsulan
comportamiento.

La sintaxis es muy simple:

#+begin_src shell
function function_name {
    # cuerpo de la función
}
#+end_src

#+REVEAL: split

A diferencia de otros lenguajes de programación, no se definen los
argumentos de la función.

Para ejecutar la función

#+begin_src shell
function_name "Hola mundo" 123
#+end_src

En este caso ="Hola mundo"= y =123= son pasados como argumentos a la
función. Estos pueden ser usados en el cuerpo de la función usando la
posición de los mismos e.g. =$1= es ="Hola mundo"=, =$2= es =123=, etc.


** Ejecutar /scripts/

Para cualquier archivo /script/ es importante que la primera línea del
archivo le diga al =shell=  que comando usar para ejecutarlo.

A la primera línea se conoce como *shebang* y se representa por =#!=
seguido de la ruta al ejecutable, e.g.:

- =#!/usr/bin/python=,
- =#!/bin/bash=,
- =#!/usr/bin/env Rscript=,
-  etc.


Sin el =shebang=, para ejecutar el archivo =ejemplo.py= debes de hacer:

#+begin_src shell
python ejemplo.py
#+end_src

pero, si agregamos el =shebang= [fn:28], puedes ejecutar el archivo de
la siguiente manera:

#+begin_src shell
./ejemplo.py
#+end_src

Te preguntarás ¿Cómo conecto estos /scripts/ con los demás usando =|=, =>=, etc?

Sencillo: Hay que modificar nuestros /scripts/ de =python=, =R= y =bash= para
leer del =stdin=.

** Python, leyendo de =stdin=

Un ejemplo mínimo de =python= es el siguiente:

#+begin_src jupyter-python :session pfds :eval no
#!/usr/bin/env python

import sys

def process(linea):
    linea = int(linea.strip())
    print(f"El triple de {linea} es {linea*3}")

for linea in sys.stdin:
    process(linea)
#+end_src

Abre =nano=, copia este código y guarda el archivo como
=script.py=. Un ejemplo de uso es el que sigue:

#+begin_src shell
seq 1 1000 | script.py
#+end_src


** R, leyendo de =stdin=

El ejemplo en =R= se ve así:

#+begin_src R :eval no
#!/usr/bin/env Rscript

f <- file("stdin")

x <- c()

open(f)

while(length(line <- readLines(f, n = 1)) > 0) {

  x <- c(x, as,numeric(line))
  print(summary(x))
}


close(f)

print("Final summary:")
summary(x)
#+end_src

#+REVEAL: split

Ejemplo de uso:

#+begin_src shell
< /data/numbers.txt script.R
#+end_src


** Tarea                                                   :ignore:homework:

#+begin_homeworkProblem

Crearemos un /script/ que analice todos los datos (de 1990 a la fecha) de la página web de
UFOs.

El cascarón de este archivo se encuentra en =scripts/ufo-analysis.sh=. Úsalo
como punto de partida.


Agrega las funciones:

1. =clean_data= Convierte a minúsculas, separador a =|=
2. =concat_data= Concatena todos los meses en un solo archivo.
3. =calculate_stats= Calcula los conteos por estado, color, forma,
   año, mes y hora
4. Crea un /script/ de =python= o =R= que genere las gráficas de estos
   conteos, guarda estos archivos.

#+end_homeworkProblem


:TEACHERONLY:

Aunque *NO* es recomendado, las páginas web de UFO tienen un =HTML= lo
suficientemente simple para poder extraer la tabla
#+begin_src shell
cat ndxe202008.html \
    | sed '1,/\<TBODY/d' \         # Borra Todo hasta la tabla
    | sed '/TBODY/,$d' \           # Borra del final de la tabla al final del archivo
    | sed 's/<[F|T][^>]\+>//g' \   # Borra las etiquetas FONT, TR, TD,
    | sed 's/<\/TD>/|/' \          # Cambia los finales de TD por |
    | sed '/^[[:space:]]*$/d' \    # Remueve las líneas vacías
    | tr -s '\r\n' ' ' \           # Remueve los saltos de línea
    | sed 's/<\/TR>/\n/g' \        # Convierte el final de la etiqueta TR por \n
    | sed 's/| $//'                # Remueve el último pipe
    | sed 's/<A.HREF=\(.*\)>\(.*\)<\/A>/\1|\2/' # Divide el URL del reporte de la fecha
#+end_src


#+begin_src shell :tangle ../scripts/ufo-analysis.sh
#! /bin/bash

BASE_URL="http://www.nuforc.org/webreports/ndxe"

function extract_table() {
    sed '1,/\<TBODY/d'  | sed '/TBODY/,$d' | sed 's/<[F|T][^>]\+>//g' | sed 's/<\/TD>/|/' | sed '/^[[:space:]]*$/d' | tr -s '\r\n' ' ' | sed 's/<\/TR>/\n/g'  | sed 's/| $//' | sed 's/<A.HREF=\(.*\)>\(.*\)<\/A>/\1|\2/'
}

function to_local_raw() {
    for url in "${BASE_URL}"{1960..2019}{01..12}".html"
    do
        curl ${url} -O -J # Nombra el archivo justo como en el
    done
}

function to_local_table() {
    for url in "${BASE_URL}"{1960..2019}{01..12}".html"
    do
        curl --stderr - ${url} | extract_table >> all.psv
    done
}

function to_table() {
    for file in *.html
    do
        < ${file} extract_table # Si intentas con cat es muy lento ...
    done
}


function calculate_stats() {
    awk -F'|' '
    BEGIN{}
    {}
    END{}
    '
}
#+end_src

:END:



* TODO Máquinas remotas                                                    :cli:
:LOGBOOK:
- State "TODO"       from              [2020-08-13 Thu 00:23]
:END:

** Secure shell

- [[https://en.wikipedia.org/wiki/Secure_Shell][*Secure shell]]* o =ssh= es un protocolo de comunicación por la red, que permite el /login/
remoto con cifrado /end-to-end/ basado en cifrado asimétrico.

- Es lo que estamos para conectarnos con la máquina virtual usando =vagrant=


* Footnotes


[fn:28] Para que el ejemplo funcione es necesario dar permisos de ejecución al
archivo =chmod u+x ejemplo.py=. =chmod= proviene de /change modifier/


[fn:27] La bandera =-n= elimina la impresión a pantalla.


[fn:25] De verdad, no sé si esto sea una palabra

[fn:24] Ni siquiera intenté traducirlo

[fn:23] En =awk= /lingo/ una línea es un /record/

[fn:22] i.e. nuestros viejos amigos los /dataframes/

[fn:21] Como discutimos anteriormente el /glob/ =*= es diferente al
/operador/ =*=

[fn:20] =HTTP= es el protocolo de comunicación usado por el navegador
para solicitar y recibir documentos guardados en otras computadoras o
servidores (/páginas web/, les dicen). Más adelante en el curso lo
discutiremos en profundidad.



[fn:15] =diff= muestra la diferencia entre dos archivos

[fn:14] Recuerda, en =GNU/Linux= todo es un archivo, incluído el /hardware/

[fn:13] En este punto es bueno preguntarse si no deberías hacerlo
mejor en otro lenguaje de programación, como =python=.

[fn:12] El sistema operativo está lleno de [[https://en.wikipedia.org/wiki/Device_file][construcciones similares]],
por ejemplo =/dev/random=


[fn:10] Por cierto, *no* creo que haya vida extraterreste,
principalmente debido a la [[https://en.wikipedia.org/wiki/Fermi_paradox][paradoja de Fermi]] (también ver
[[https://waitbutwhy.com/2014/05/fermi-paradox.html][aquí]]). Relacionado con esto, creo que la mejor solución a esta
paradoja es la teoría del [[https://mason.gmu.edu/~rhanson/greatfilter.html][Gran Filtro]] ([[https://wiki.lesswrong.com/wiki/Great_Filter][más información]]). Si prefieres
vídeos este [[https://www.youtube.com/playlist?list=PLIIOUpOge0LulClL2dHXh8TTOnCgRkLdU][playlist]] tiene todo lo que quieres saber.


[fn:8] ¿Recuerdas el *REPL*?
