#+TITLE: Desarrollo de Software
#+DATE: Primavera 2020

#+LANGUAGE: es

#+PROPERTY: header-args :eval never-export
#+STARTUP: latexpreview
#+STARTUP: overview
#+STARTUP: hideblocks

#+EXCLUDE_TAGS: slidesonly teachersonly noexport homework appendix

#+OPTIONS: TeX:t LaTeX:t
#+OPTIONS: d:(not "TEACHERONLY" "ZOOM" "LOGBOOK")

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="/programacion-para-ciencia-de-datos/css/gtd.css" />
#+OPTIONS: email:nil num:nil author:nil tags:nil timestamp:nil toc:nil todo:t ^:nil html-scripts:nil html-style:nil


#+KEYWORDS: software python
#+DESCRIPTION:

* Desarrollo de Software
#+INCLUDE: programming.org

** SOLID

#+INCLUDE: "../notas/solid.org" :minlevel 2 :only-contents t :lines "12-"

* Ejemplo: TSP

#+INCLUDE: "../notas/jonesAIApplicationProgramming2003.org::*TSP" :only-contents t

** El código que hay que convertir ...

#+INCLUDE: "../notas/jonesAIApplicationProgramming2003.org::*Primera iteración"  :only-contents t

** Hacia un producto de datos

#+INCLUDE: "../notas/jonesAIApplicationProgramming2003.org::*Requerimientos"  :only-contents t


#+INCLUDE: "../notas/jonesAIApplicationProgramming2003.org::*Diseño"  :only-contents t

#+INCLUDE: "../notas/jonesAIApplicationProgramming2003.org::*Pruebas unitarias"  :only-contents t

*** Algoritmos

**** Algoritmos /greedy/

Si nuestra estrategia de optimización se basa sólo en información
/local/, se conoce como estrategia (o algoritmo) /greedy/. En el caso del
TSP, la característica de "local" se presenta cuando, dado el lugar
donde está nuestro viajero, decidimos el siguiente lugar, únicamente
en el costo de llegar a él desde la posición actual del viajero.

Regularmente esta familia de algoritmos no da resultados óptimos, pero
es fácil de implementar y es rápida al ejecutar.

**** Ant algorithms
#+INCLUDE: "../notas/jonesAIApplicationProgramming2003.org::*Descripción"  :only-contents
