from words import get_number_of_words

def test_get_number_of_words():
    assert get_number_of_words('Hola mundo') == 2
