#+REVEAL_INIT_OPTIONS: width:1200, height:800, margin: 0.1, minScale:0.2, maxScale:2.5, transition:'fade'
#+REVEAL_ROOT: ../reveal.js
#+REVEAL_THEME: nano-reveal
#+REVEAL_HLEVEL: 1

#+REVEAL_PLUGINS: (notes highlight search zoom)

#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Programación para Ciencia de Datos">
#+REVEAL_POSTAMBLE: <p> Creado por Adolfo De Unánue. </p>

#+REVEAL_EXTRA_CSS: ../css/presentation.css
#+REVEAL_HIGHLIGHT_CSS: ../css/solarized-light.css

#+REVEAL_SLIDE_FOOTER: <br>
#+OPTIONS: reveal_toc_footer:t

# Tamaño de los slides
#+OPTIONS: reveal_width:1200 reveal_height:900

# Show page number and total number of slides
#+OPTIONS: reveal_slide_number:c/t



#+OPTIONS: reveal_klipsify_src:nil

# Usar AUTHOR, DATE y EMAIL desde los org
#+OPTIONS: author:t date:t email:t
# Table of Contents
#+OPTIONS: toc:nil
# Número de header
#+OPTIONS: num:nil
# Sin página de título
#+OPTIONS: reveal_title_slide:nil
# Smart quotes
#+OPTIONS: ':t

#+EXCLUDE_TAGS: handbookonly noexport
#+INCLUDE_TAGS: slidesonly

#+INCLUDE: title-slide.org


* Macros                                                             :ignore:
#+MACRO: understandingquestion What did you find difficult or confusing about the *contents* of the presentation?  Please be as specific as possible.
